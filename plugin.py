#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:ts=4:sw=4:softtabstop=4:smarttab:expandtab

# target script

import sys
import re

#from tkinter import *

def request_balise():
    fen = Tk()
    Label(fen, text="Balise").grid(row=0)
    Label(fen, text="Class").grid(row=1)
    
    balise = Entry(fen)
    class_val = Entry(fen)

    balise.grid(row=0, column=1)
    class_val.grid(row=1, column=1)

    Button(fen, text='OK', command=fen.quit).grid(row=3, column=0, sticky=W, pady=4)
    fen.mainloop()
	
    balise = balise.get()
    class_val = class_val.get()
	
    metadonnee = [balise, class_val]

    return metadonnee


def roman_siecle(bk):
    #balise_value = request_balise()
    #balise = balise_value[0]
    balise = "span"
    #class_value = balise_value[1]
    class_value = "CharOverride-10"
    
    data =""
    for (id, href) in bk.text_iter():
        
        data = bk.readfile(id)
        
        reg = re.compile(r'(?P<b1>(<'+balise+' class="(([a-zA-Z0-9_-]+) |)'+class_value+')(| ([a-zA-Z0-9_-]+))">)(?P<roman>([xiv]+))(?P<b2>(</'+balise+'>))')
		
        search = reg.search(data)
        print(search)
        if search != None:
            balise1  = search.group('b1')
            balise2 = search.group('b2')
            roman = search.group('roman')
				
            sub_str = balise1+"<span style=\"font-size:83%\">"+roman.upper()+"</span>"+balise2
			
            test2 = data.replace(balise1+roman+balise2, sub_str)
            data.replace(balise1+roman+balise2, sub_str)
            data2=re.sub(reg, sub_str, data)
			
            #effacer le fichier ancien
            bk.deletefile(id)
            basename = id+".xhtml"
            mt = "application/xhtml+xml"
            uid1 = id
			
            #ajouter un nouveau fichier
            bk.addfile(uid1, basename, data2, mt)
			
def PCapH2(bk):
    data =""
    for (id, href) in bk.text_iter():
        data = bk.readfile(id)
        reg = re.compile(r'(?P<b1>(<h2 class="T3" id="sigil_toc_id_([0-9]+)" xml\:lang="fr\-CH"><span class="CharOverride\-2">))(?P<content>(.*))(?P<b2>(<\/span><\/h2>))')
        search = reg.search(data)
        if search != None:
            print(search)
            Content = search.group('content')
            minReg = re.compile(r'(?P<minuscule>[a-zàâäéèêëûüôöîïÿ]+)')
            minSearch = minReg.search(Content)
            if minSearch != None:

                minuscule = minSearch.group('minuscule')
                print('Minuscule : '+minuscule)
                a_remp = minSearch.group(0)+"<span style=\"font-size:83%\">" + minuscule.upper() + "</span>"+minSearch.group(1)

                minReg.search(Content)

                print(a_remp)
                Content = re.sub(minReg, a_remp, Content)
                #Content2 = re.sub(minReg, a_remp, Content)
                #print(Content)
				
                #bk.deletefile(id)
                #basename = id+".xhtml"
                #mt = "application/xhtml+xml"
                #uid1 = id

                #ajouter un nouveau fichier
                #bk.addfile(uid1, basename, Content, mt)
				
                sub_str = "<h2 class=\"T3\" xml:lang=\"fr-CH\"><span class=\"CharOverride-2\">"+Content+"</span></h2>"
                print(sub_str)                                                                
                #test2 = data.replace(Content, sub_str)
                #data.replace(balise1+roman+balise2, sub_str)
                data2 = re.sub(reg, sub_str, data)
                  
                #effacer le fichier ancien
                bk.deletefile(id)
                basename = id+".xhtml"
                mt = "application/xhtml+xml"
                uid1 = id

                #ajouter un nouveau fichier
                bk.addfile(uid1, basename, data2, mt)

def run(bk):
    #roman_siecle(bk)
    PCapH2(bk)
	
 
def main():
    print("I reached main when I should not have\n")
    return -1
    
if __name__ == "__main__":
    sys.exit(main())

