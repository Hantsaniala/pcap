#!/usr/bin/python
# -*- coding: utf-8 -*-

import optparse #Console
import re #regex


#Pour les arguments a entrer en paramèttre dans la console
def initParam():
    parser = optparse.OptionParser("usage %prog -f <filename> -b <balise> -c <class>")
    parser.add_option('-f', dest='fname', type='string', help='spécifié un fichier')
    parser.add_option('-b', dest='bname', type='string', help='spécifié une balise')
    parser.add_option('-c', dest='cval', type='string', help='spécifié une classe')
    (options, args) = parser.parse_args()

    if (options.fname == None) | (options.bname == None) | (options.cval == None):
        print (parser.usage)
        exit(0)

    else:
        file_name = options.fname
        balise_name = options.bname
        class_value = options.cval
        return (file_name, balise_name, class_value)

#Fonction principale du script
def main():
    param_value = initParam()
    file_name = param_value[0]
    balise_name = param_value[1]
    class_value = param_value[2]

    file_data = open(file_name, 'r') #Ouverture fichier source
        
    filename = file_name.split(".") #pour le nom du fichier de destination
        
    Création du fichier de destination
    file_result = open(filename[0]+"-OK."+filename[1], "wx")
    file_result.close()
        
    for line in file_data.readlines():
    
        #print(line)
        reg = re.compile(r'(?P<b1>(<'+balise_name+' class="(([a-zA-Z0-9_\-]*?) |)'+class_value+')(| ([a-zA-Z0-9_\-]*?))">)(?P<maj>([A-ZÀÁÂÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ]))(?P<min>([a-zàáâãäåæçèéëìíîïðòóôõöøùúûüýÿµœš]*?))(?P<b2>(<\/'+balise_name+'>))')
        #print(reg)
        search = reg.findall(line)

        print(search)

        if (search != None):
            for matchObj in search:
                
                print(matchObj)
                #print(matchObj[8])
                print(line)
                
                balise1  = matchObj[0] #search.group('b1')
                print(balise1)
                
                balise2 = matchObj[10] #matchObj.group('b2')
                print(balise2)
                
                maj = matchObj[6] #matchObj.group('maj')
                print(maj)
                
                min = matchObj[8] #matchObj.group('min')
                print(min)
                    
                sub_str = balise1+""+maj+"<span style=\"font-size:83%\">"+min.upper()+"</span>"+balise2

                line = re.sub(reg, sub_str, line)

                #ouverture du fichier pour ajour ligne
                filename = file_name.split(".")
                file_result = open(filename[0]+"-OK."+filename[1], "wx")
                file_result.writelines(line)

                file_data.writelines(line)
                file_result.close()

                print(line)
                 
        else:
            filename = file_name.split(".")
            #file_result = open(filename[0]+"-OK."+filename[1], "a")

            #file_result.writelines(line)
            #file_result.close()
       
        file_data.close()

#Dans le cas ou le script et lancé via un import        
if __name__ == '__main__':
    main()