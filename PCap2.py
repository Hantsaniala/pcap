﻿#!/usr/bin/python
# -*- coding: utf-8 -*-

import optparse #Console
import re #regex
from bs4 import BeautifulSoup, Tag #HTML parser
from html5lib import *
import glob
import os
import chardet


def about():
    print("""
####################################################
               =- PCap v1.0 -=
####################################################
          
Script pour traiter automatiquement la plupart 
des PCap dans les fichiers XHTML de certain ePub.
          
Dernière version Novembre 2015.
             
             
           (c) Hantsaniala Eléo 2015
         Eléo Rh <hantsaniala@gmail.com>
            
            
          """)

#Pour les arguments a entrer en paramèttre dans la console
def initParam():
    parser = optparse.OptionParser("Utilisation : python PCap2.py -d <file_dir> | -f <file_name> -b <balise> -c <class>")
    parser.add_option('-d', dest='dname', type='string', help='spécifié une répertoire')
    parser.add_option('-f', dest='fname', type='string', help='spécifié un fichier')
    parser.add_option('-b', dest='bname', type='string', help='spécifié une balise')
    parser.add_option('-c', dest='cval', type='string', help='spécifié une classe')
    (options, args) = parser.parse_args()

    if (options.bname == None):
        print ("[i] Veuillez spécifier un nom de balise !")
        print (parser.usage)
        exit(0)
        
    elif (options.cval == None):
        print ("[i] Veuillez spécifier une classe au moins !")
        print (parser.usage)
        exit(0)
        
    elif (options.dname == None) & (options.fname == None):
        print ("[i] Veuillez spécifier une répertoire ou un nom de fichier !")
        print (parser.usage)
        exit(0)
        
    else:
        dir_name = options.dname
        file_name = options.fname
        balise_name = options.bname
        class_value = options.cval
        
        return (dir_name, file_name, balise_name, class_value)

def findWordWithoutSpace(soup, balise_name, class_value):
	for span in soup.find_all(balise_name, attrs={'class': class_value}):
		span_attrs = span.attrs['class']
		attrib = ""
		for attribut in span.attrs['class']:
		  attrib += " "+attribut
		stringTest = re.compile(r"[ \-]") #Pour eviter les fichiers avec espaces
		if(stringTest.search(span.text) == None):
			regex = re.compile(r"(?P<maj>([A-ZÀÁÂÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝ]))(?P<min>([a-zàáâãäåæçèéëìíîïðòóôõöøùúûüýÿµœš]+))")
			search = regex.search(span.text)
			if (search != None):
				min = search.group('min')
				maj = search.group('maj')

				#Création du tag pour la taille 83%
				isUpper = soup.new_tag("span")
				isUpper.attrs = {"style":"font-size:83%"}
				isUpper.string = min.upper() #min to maj

				sub_str = maj+""+str(isUpper)
				PCap = re.sub(regex, sub_str, span.text)

				#Création du tag pour garder l'ancien tag
				isPCap = soup.new_tag("span")
				isPCap.attrs = {"class" : attrib}
				isPCap.string = PCap

				span = span.replace_with(isPCap)

				span.encode(formatter="html")
				
			else: #Dans le cas hoe minuscule daholo
				regex2 = re.compile(r"(?P<min>([a-zàáâãäåæçèéëìíîïðòóôõöøùúûüýÿµœš]+))")
				search2 = regex2.search(span.text)
				if (search2 != None):
					min = search2.group('min')

					#Création du tag pour la taille 83%
					isUpper = soup.new_tag("span")
					isUpper.attrs = {"style":"font-size:83%"}
					isUpper.string = min.upper() #min to maj

					sub_str = str(isUpper)
					PCap = re.sub(regex2, sub_str, span.text)

					#Création du tag pour garder l'ancien tag
					isPCap = soup.new_tag("span")
					isPCap.attrs = {"class" : attrib}
					isPCap.string = PCap

					span = span.replace_with(isPCap)

					span.encode(formatter="html")
			
		else: #Dans le cas de présence d'éspsace
			#findWordWithPonct(soup, span, attrib)
			continue

#Fonction de traitement pour un seul fichier
def dealFile(file_name, balise_name, class_value):
  print("\n\n[+] Ouverture du fichier source "+file_name+"...")
  file_data = open(file_name, 'r') #Ouverture fichier source

  filename = file_name.split(".") #pour le nom du fichier de destination


  #Création du fichier de destination
  print("[+] Création du fichier de déstination \""+filename[0]+"-OK."+filename[1] +"\"...")
  file_result = open(filename[0]+"-OK."+filename[1], "w")
  file_result.close()
    
  try:
    soup = BeautifulSoup(open(file_name, encoding="utf16"), "html5lib")
    encodage = chardet.detect(soup)['encoding']
    #print("Encodage : "+encodage)
    #Dans le cas de plusieurs class en même temps
    for subClass_value in class_value.split(" "):
      print("[+] Traitement du fichier pour la classe "+subClass_value+"...")
      findWordWithoutSpace(soup, balise_name, subClass_value)
        
    #ouverture du fichier pour ajour ligne
    filename = file_name.split(".")
    file_result = open(filename[0]+"-OK."+filename[1], "a")
    print("[+] Ecriture du contenu...")
    file_result.writelines(str(soup.encode('utf8')))

    file_result.close()
    print("[+] Traitement éfféctué avec succès !! (^_^)")

  except Exception as e:
    print("/!\ Erreur : "+str(e))
    print("[i] Problème d'encodage ou de caractère. \nSOLUTION : Convertir le fichier en \"ANSI\", enregistrer, puis en \"utf-16-le\", enregistrer et relancer le script !")
    print("[+] Annulation des taches en cours...")
        
#Fonction principale du script
def main():
  about()
  param_value = initParam()
  dir_name = param_value[0]
  file_name = param_value[1]
  balise_name = param_value[2]
  class_value = param_value[3]
  
  if (dir_name != None):
    #Ajout de fonction pour éffectuer les taches sur toutes les fichiers du repertoire
    file_list = glob.glob(os.path.join(dir_name, "*.xhtml"))
    for file_name in file_list:
      dealFile(file_name, balise_name, class_value)
  elif (file_name != None):
    dealFile(file_name, balise_name, class_value)
  else :
    print("[i] Veuillez spécifier une répertoire ou un nom de fichier !")
	

#Dans le cas ou le script et lancé via un import        
if __name__ == '__main__':
	main()
